package com.vesodev.dndtools.model;

/**
 * Created by vesko on 7.7.2021 г..
 */
public interface DnDCampaignView {

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);
}
