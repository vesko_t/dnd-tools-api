package com.vesodev.dndtools.model;

/**
 * Created by vesko on 24.6.2021 г..
 */
public interface CharacterView {

    public Integer getId();

    public String getRace();

    public String getName();

    public Integer getLevel();
}
