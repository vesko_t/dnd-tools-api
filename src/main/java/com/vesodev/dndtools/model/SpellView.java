package com.vesodev.dndtools.model;

/**
 * Created by vesko on 24.6.2021 г..
 */
public interface SpellView {

    public Integer getId();

    public String getName();

    public Integer getSpellSlot();
}
