package com.vesodev.dndtools.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Entity
public class Dice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private DiceType type;

    private Integer count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DiceType getType() {
        return type;
    }

    public void setType(DiceType type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
