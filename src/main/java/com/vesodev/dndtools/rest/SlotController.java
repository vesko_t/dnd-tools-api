package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.SpellSlotRepository;
import com.vesodev.dndtools.model.SpellSlot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vesko on 24.6.2021 г..
 */
@RestController
public class SlotController {

    @Autowired
    private SpellSlotRepository spellSlotRepository;

    @PutMapping(path = "/slots")
    public SpellSlot saveSlot(@RequestBody SpellSlot spellSlot) {
        return this.spellSlotRepository.save(spellSlot);
    }

}
