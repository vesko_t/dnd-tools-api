package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.StatRepository;
import com.vesodev.dndtools.model.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vesko on 24.6.2021 г..
 */
@RestController
public class StatController {

    @Autowired
    private StatRepository statRepository;

    @PutMapping(path = "/stats")
    public Stat saveSlot(@RequestBody Stat stat) {
        return this.statRepository.save(stat);
    }
}
