package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Item;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 22.5.2021 г..
 */
public interface ItemRepository extends CrudRepository<Item, Integer> {
}
