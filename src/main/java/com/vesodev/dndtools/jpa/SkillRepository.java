package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Skill;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 22.5.2021 г..
 */
public interface SkillRepository extends CrudRepository<Skill, Integer> {
}
