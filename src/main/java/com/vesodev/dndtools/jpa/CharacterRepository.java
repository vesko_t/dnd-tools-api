package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Character;
import com.vesodev.dndtools.model.CharacterView;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vesko on 22.5.2021 г..
 */
public interface CharacterRepository extends CrudRepository<Character, Integer> {

    public Character findByName(String name);

    public List<Character> findAllByNameLike(String name);

    //@Query(value = "select id, name, level, char_class FROM CHARACTER where player_name=:playerName", nativeQuery = true)
    public List<CharacterView> findAllByPlayerName(@Param("playerName") String playerName);
}
