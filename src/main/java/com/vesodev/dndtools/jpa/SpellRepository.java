package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Spell;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vesko on 22.5.2021 г..
 */
public interface SpellRepository extends CrudRepository<Spell, Integer> {

    @Query(value = "select * from Spells where classes like CONCAT('%',:classes,'%') and spell_slot <= :spellSlot order by spell_slot, name", nativeQuery = true)
    public List<Spell> findByClassesContainsAndSpellSlotLessThanEqual(@Param("classes") String classes, @Param("spellSlot") Integer spellSlot);

    @Query(value = "select * from Spells order by spell_slot, name", nativeQuery = true)
    List<Spell> getAll();

    @Query(value = "select * from Spells where name like concat('%',:name,'%') order by spell_slot, name", nativeQuery = true)
    List<Spell> findAllByNameContains(@Param("name") String name);
}
